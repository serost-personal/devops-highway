
terraform {
  required_providers {
    google = {
      source = "google"
    }
  }
}

variable "gcp_cred_file" {
  description = "Credentials file for GCP authentication"
}

variable "gcp_project_id" {
  description = "Project within GCP to operate with"
}

variable "gcp_region" {
  description = "Region within GCP to operate with"
}

provider "google" {
  # credentials are linked to some service account
  # make sure it has "Browser" and "Compute Admin" roles
  credentials = file(var.gcp_cred_file)
  project     = var.gcp_project_id
  region      = var.gcp_region
}

resource "google_compute_instance" "default" {
  name         = "some-vm"
  machine_type = "e2-micro"
  zone         = "europe-west3-b"
  tags         = ["ssh"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
  }
}
